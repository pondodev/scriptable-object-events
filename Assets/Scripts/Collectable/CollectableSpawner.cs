using UnityEngine;
using Random = UnityEngine.Random;

public class CollectableSpawner : MonoBehaviour
{
    [SerializeField] private GameObject collectable;
    [SerializeField] private int spawnIncreaseAmount;
    [SerializeField] private int maxSpawnAmount;
    [SerializeField] private int maxCollectablesInScene;

    private float spawnAmount = 1;
    private int collectableCount = 1;
    private Camera mainCamera;

    void Start()
    {
        // i get this is gross but this is just a quick thing leave me alone
        mainCamera = Camera.main;
    }

    public void SpawnCollectables()
    {
        collectableCount--;
        
        for (int i = 0; i < spawnAmount; i++)
        {
            if (collectableCount == maxCollectablesInScene) break; // we can't spawn more once we hit the max
            
            Vector2 screenMin = Vector2.zero;
            Vector2 screenMax = new Vector2(Screen.width, Screen.height);
            Vector2 screenPos = new Vector2(
                Random.Range(screenMin.x, screenMax.x),
                Random.Range(screenMin.y, screenMax.y));

            Vector3 spawnPos = mainCamera.ScreenToWorldPoint(screenPos);
            spawnPos.z = 0;
            Instantiate(collectable, spawnPos, Quaternion.Euler(Vector3.zero));
            collectableCount++;
        }

        spawnAmount += spawnIncreaseAmount;
        spawnAmount = Mathf.Min(spawnAmount, maxSpawnAmount);
    }
}
