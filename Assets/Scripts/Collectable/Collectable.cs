using UnityEngine;

public class Collectable : MonoBehaviour
{
    [SerializeField] private GameEvent gameEvent;
    [SerializeField] private GameObject destroyEffect;
    
    void OnTriggerEnter2D(Collider2D other)
    {
        gameEvent.Raise();
        
        // TODO: put audio for picking up collectable here!

        Instantiate(destroyEffect, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
