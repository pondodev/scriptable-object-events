using System.Collections.Generic;
using Player;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Event", fileName = "New Game Event")]
public class GameEvent : ScriptableObject
{
    private List<GameEventListener> _gameEventListeners = new List<GameEventListener>();

    public void Raise()
    {
        for (int i = _gameEventListeners.Count - 1; i >= 0; i--)
            _gameEventListeners[i].OnEventRaised();
    }

    public void RegisterListener(GameEventListener listener)
    {
        _gameEventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListener listener)
    {
        _gameEventListeners.Remove(listener);
    }
}
