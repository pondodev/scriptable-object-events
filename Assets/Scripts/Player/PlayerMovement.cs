using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    
    private Rigidbody2D _rb;
    
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    public void Move(Vector3 direction)
    {
        var newPos = transform.position + direction * (moveSpeed * Time.fixedDeltaTime);
        _rb.MovePosition(newPos);
    }
}
