using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Vector3 GetMoveDirection()
    {
        var h = Input.GetAxisRaw("Horizontal");
        var v = Input.GetAxisRaw("Vertical");
        
        return new Vector3(h, v, 0f).normalized;
    }
}
