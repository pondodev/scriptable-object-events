using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(PlayerInput), typeof(PlayerMovement))]
    public class PlayerManager : MonoBehaviour
    {
        private PlayerInput _playerInput;
        private PlayerMovement _playerMovement;
        
        private Vector3 _moveDir;
        
        void Start()
        {
            _playerInput = GetComponent<PlayerInput>();
            _playerMovement = GetComponent<PlayerMovement>();
        }

        void Update()
        {
            _moveDir = _playerInput.GetMoveDirection();
        }

        void FixedUpdate()
        {
            _playerMovement.Move(_moveDir);
        }
    }
}
