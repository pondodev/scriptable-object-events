using UnityEngine;
using TMPro;

public class MainUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreDisplay;
    [SerializeField] private Score score;

    void Start()
    {
        score.value = 0;
        DisplayScore();
    }

    public void IncreaseScore()
    {
        score.value++;
        DisplayScore();
    }

    private void DisplayScore()
    {
        if (score.value < 10000)
            scoreDisplay.SetText(score.value.ToString());
        else
            scoreDisplay.SetText("wtf");
    }
}
