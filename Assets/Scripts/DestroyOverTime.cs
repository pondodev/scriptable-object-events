using UnityEngine;

public class DestroyOverTime : MonoBehaviour
{
    [SerializeField] private float delay;
    void Start()
    {
        Destroy(gameObject, delay);
    }
}
