using UnityEngine;

[CreateAssetMenu(menuName = "Score", fileName = "New Score")]
public class Score : ScriptableObject
{
    public int value;
}
